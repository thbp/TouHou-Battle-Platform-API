# 东方对战平台API
* [swagger-ui 3 版本](https://api.thbp.online/)
* [swagger-ui 2 中文版本](https://api.thbp.online/zh-cn)
* [swagger-editor 在线编辑器](https://editor.api.thbp.online) 每次使用前，点击File-Open Example-Open，清空缓存。编辑器内的修改不会反映到swagger-ui网页上，必须推送到此仓库。

# 对战平台联机示意图
![对战平台联机示意图](image/对战平台联机示意图.png "对战平台联机示意图")  

# 逻辑网络连接图
![逻辑网络连接图](image/网络连接图.png "逻辑网络连接图")  